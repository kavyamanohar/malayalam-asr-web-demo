## Malayalam ASR Web Demo

Demonstrating Malayalam Speech recognition using [Vosk](https://github.com/alphacep/vosk-api). The model [trained](https://gitlab.com/kavyamanohar/ml-subword-asr) using Kaldi is used at backend.

The Demo is built using [gradio](https://www.gradio.app/).

**[Try it out](https://asr.smc.org.in/)** 

The Demo allows speech recognition is three modes:

- Transcribe an audio file by uploading
- Transcribe an audio file that is recorded using microphone
- Transcribe Live in streaming mode

**Use _Clear_ button to remove previous transcript from the output text box.**

**Transcribes well articulated Malayalam speech**


### Related Projects

1. Desktop [Text Dictation Application](https://gitlab.com/kavyamanohar/dictation-app)

2. Vosk Browser Demo using Web Assembly. [Demo](https://kavyamanohar.github.io/vosk-browser/), [Repo](https://github.com/kavyamanohar/vosk-browser)

