#!/usr/bin/env python3

import json
import gradio as gr
import re
from vosk import KaldiRecognizer, Model

model = Model("./models/sbpe-4-model")

def transcribe(stream, data):

    sample_rate, audio_data = data
    audio_data = audio_data.tobytes()

    if stream is None:
        rec = KaldiRecognizer(model, sample_rate)
        result = []
    else:
        rec, result = stream

    if rec.AcceptWaveform(audio_data):
        text_result = json.loads(rec.Result())["text"]
        text_result = re.sub(r'\+ ', '', text_result)
        if text_result != "":
            result.append(text_result)
        partial_result = ""
    else:
        partial_result = json.loads(rec.PartialResult())["partial"] + " "
        partial_result = re.sub(r'\+ ', '', partial_result)
    
    return (rec, result), "\n".join(result) + "\n" + partial_result


demo = gr.Blocks(title="Malayalam Automatic Speech Recognition")
gr.Markdown("Hello")

live_transcribe=gr.Interface(
    fn=transcribe,
    inputs=[
        "state", gr.Audio(sources=["microphone"], type="numpy", streaming=True),
    ],
    outputs=[
        "state", "text",
    ],
    live=True)

rec_transcribe=gr.Interface(
    fn=transcribe,
    inputs=[
        "state", gr.Audio(sources=["microphone"], type="numpy", streaming=False),
    ],
    outputs=[
        "state", "text",
    ],
    live=False)

file_transcribe=gr.Interface(
    fn=transcribe,
    description="*You can click on the examples below to try trascribing audio files*",
    examples=[
        ["./male.wav", "transcribe", True],
        ["./female.wav", "transcribe", True]
    ],
    inputs=[
        "state", gr.Audio(sources=["upload"]),
    ],
    outputs=[
        "state", "text",
    ],
    cache_examples=False,
    live=False)

with demo:
    gr.Markdown("# Malayalam Automatic Speech Recognition")
    gr.Markdown("**Your data is never saved.** \n [Demo](https://gitlab.com/kavyamanohar/malayalam-asr-web-demo) \n [Model](https://gitlab.com/kavyamanohar/ml-subword-asr) ")
    gr.TabbedInterface([file_transcribe,rec_transcribe,live_transcribe ], ["Transcribe Audio File","Transcribe After Recording", "Transcribe Live"])

demo.launch(share=True)